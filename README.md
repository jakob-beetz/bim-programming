# bim-programming

Collection of Jupyter Notebooks for the introduction of programming Building Information Models (BIM) in python

The notebooks are used in the Bachelor track of the architecure curriculum at the RWTH Aachen and are licensed under MIT license as Open Educational Resources (OER)


![img/image.png](img/image.png)
